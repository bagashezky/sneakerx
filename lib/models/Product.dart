import 'package:flutter/material.dart';

class Product {
  final int id;
  final String title, description;
  final List<String> images;
  final List<String> colors;
  final double rating, price;
  final bool isFavourite, isPopular;

  Product({
    required this.id,
    required this.images,
    required this.colors,
    this.rating = 0.0,
    this.isFavourite = false,
    this.isPopular = false,
    required this.title,
    required this.price,
    required this.description,
  });
}

// Our demo Products

List<Product> demoProducts = [
  Product(
    id: 1,
    images: [
      "assets/images/vans/1.jpg",
      "assets/images/vans/2.jpg",
      "assets/images/vans/3.jpg",
      "assets/images/vans/4.jpg",
    ],
    colors: [
      "28",
      "29",
     "30",
     "31",
    ],
    title: "Vans Slip On",
    price: 300.129,
    description: description,
    rating: 4.8,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 2,
    images: [
      "assets/images/ortuseight/1.jpg",
      "assets/images/ortuseight/2.jpg",
      "assets/images/ortuseight/3.jpg",
      "assets/images/ortuseight/4.jpg",
      "assets/images/ortuseight/5.jpg",
    ],
    colors: [
      "28",
      "29",
     "30",
     "31",
    ],
    title: "Ortuseight - Jogosala Crusher Ivory Armygreen",
    price: 429.999,
    description: description,
    rating: 4.1,
    isPopular: true,
  ),
  Product(
    id: 3,
    images: [
      "assets/images/adidas/1.jpg",
      "assets/images/adidas/2.jpg",
      "assets/images/adidas/3.jpg",
      "assets/images/adidas/4.jpg",
      "assets/images/adidas/5.jpg",
    ],
    colors: [
      "28",
      "29",
     "30",
     "31",
    ],
    title: "Adidas Broomfield Grey Navy",
    price: 407.320,
    description: description,
    rating: 4.1,
    isFavourite: true,
    isPopular: true,
  ),
  
];

const String description =
    "Foto produk 100% Real Pict (Beda boleh dikembalikan) * Available size : 39 40 41 42 43 44 * Quality : Mirror * Sudah lengkap dengan box seperti di foto …";
