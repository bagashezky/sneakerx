import 'package:flutter/material.dart';
import 'package:shop_app/models/Cart.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class CartCard extends StatelessWidget {
  const CartCard({
    Key? key,
    required this.cart,
  }) : super(key: key);

  final Cart cart;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Row(
      children: [
        SizedBox(width: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text.rich(
            TextSpan(
                text: "Selamat, Pembayaran sukses dilakukan",
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: Color.fromARGB(255, 0, 0, 0)),
            ),
            ),
            SizedBox(height: 10),
            Text.rich(
              TextSpan(
                text: " \Rp${cart.product.price}",
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: kPrimaryColor),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
