import 'package:flutter/material.dart';
import 'package:shop_app/models/Cart.dart';

import 'package:shop_app/ui/size_config.dart';

import 'components/body.dart';
import 'components/check_out_card.dart';

class NotifScreen extends StatelessWidget {
  static String routeName = "/notif";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: buildAppBar(context),
      body: Body(),
     // bottomNavigationBar: CheckoutCard(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Column(
        children: [
          Text(
            "Notifikasi",
            style: TextStyle(color: Colors.black),
          ),
          Text(
            "${demoCarts.length} items",
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}
